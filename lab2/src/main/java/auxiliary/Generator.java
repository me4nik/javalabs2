package auxiliary;

import auxiliary.directoryActions.*;
import auxiliary.fileActions.*;
import filesystem.*;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Generator {
    private static void printInfo(Controller controller, File file) throws ExecutionException, InterruptedException {
        System.out.println("--------");
        Future<String> future = controller.submitFSAction(new GetNameAction(file));
        System.out.println("Name: " + future.get());
        future = controller.submitFSAction(new GetPathAction(file));
        System.out.println("Path: " + future.get());
        future = controller.submitFSAction(new GetDescriptionAction(file));
        System.out.println("Description: " + future.get());
        System.out.println("--------");
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        Directory root = new Directory("root");
        Controller controller = new Controller(10);
        Random random = new Random();

        for(int i = 0; i < 50; i++){

            Directory dir = root;

            if(random.nextInt( 50) % 2 == 0){
                dir = new Directory("Dir" + i);
                controller.submitFSAction(new InsertFileAction(root, dir));
            }
            if (random.nextInt( 50) % 3 == 0) {
                BinaryFile binaryFile = new BinaryFile("Binary" + i);
                controller.submitFSAction(new InsertFileAction(dir, binaryFile));
                printInfo(controller, binaryFile);
            }
            if (random.nextInt( 50) % 3 == 0) {
                Buffer buffer = new Buffer("Buffer" + i, i);
                controller.submitFSAction(new InsertFileAction(dir, buffer));
                printInfo(controller, buffer);
                if (i % 2 == 0) {
                    System.out.println("Delete " + controller.submitFSAction(new GetNameAction(buffer)).get());
                    controller.submitFSAction(new DeleteAction(buffer));
                }
            }
            if (random.nextInt( 50) % 3 == 0) {
                Log log = new Log("Log" + i);
                controller.submitFSAction(new InsertFileAction(dir, log));
                printInfo(controller, log);
                if (random.nextInt( 50) % 3 == 0) {
                    Directory subDir = new Directory("SubDir2_" + i);
                    controller.submitFSAction(new InsertFileAction(dir, subDir));
                    controller.submitFSAction(new MoveAction(log, subDir));
                }
            }
        }

        String dirName = "Dir1";
        Future<List<String>> searchFuture = controller.submitFSAction(new SearchAction(root, dirName));
        System.out.println(">>>>> Search result for: " + dirName);
        for (String search: searchFuture.get()) {
            System.out.println(search);
        }

        Future<Long> countFuture = controller.submitFSAction(new CountAction(root, true));
        System.out.println(">>>>>>> Count: " + countFuture.get());

        Future<String> treeFuture = controller.submitFSAction(new TreeAction(root));
        System.out.println("************************* TREE **********************************");
        System.out.println(treeFuture.get());

        Future<List<File>> sortFuture = controller.submitFSAction(new SortAction(root));
        System.out.println("************************* SORTED **********************************");
        for (File f: sortFuture.get()) {
            System.out.println(f.getName());
        }

        controller.stop();
    }
}