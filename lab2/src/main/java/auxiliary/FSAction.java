package auxiliary;

public interface FSAction<T> {
    T execute();
}