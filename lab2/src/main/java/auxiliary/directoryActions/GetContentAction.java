package auxiliary.directoryActions;

import auxiliary.FSAction;
import filesystem.Directory;
import filesystem.File;

import java.util.List;

public class GetContentAction implements FSAction<List<File>> {
    private final Directory directory;

    public GetContentAction(Directory directory) {
        this.directory = directory;
    }

    @Override
    public List<File> execute() {
        return directory.getContent();
    }
}

