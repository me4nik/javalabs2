package auxiliary.directoryActions;

import auxiliary.FSAction;
import filesystem.Directory;

public class CountAction implements FSAction<Long> {
    private final Directory directory;
    private final Boolean recursive;

    public CountAction(Directory directory,  Boolean recursive) {
        this.directory = directory;
        this.recursive = recursive;
    }


    @Override
    public Long execute() {
        return directory.count(recursive);
    }
}
