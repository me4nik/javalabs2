package auxiliary.directoryActions;

import auxiliary.FSAction;
import filesystem.Directory;

import java.io.IOException;

public class TreeAction implements FSAction<String> {
    private final Directory directory;

    public TreeAction(Directory directory) {
        this.directory = directory;
    }

    @Override
    public String execute() {
        try{
            return directory.tree();
        }catch (IOException e){
            return "ERROR: " + e.getMessage();
        }
    }
}