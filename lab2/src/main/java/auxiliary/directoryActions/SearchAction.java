package auxiliary.directoryActions;

import auxiliary.FSAction;
import filesystem.Directory;

import java.util.List;

public class SearchAction implements FSAction<List<String>> {
    private final Directory directory;
    private final String pattern;

    public SearchAction(Directory directory, String pattern) {
        this.directory = directory;
        this.pattern = pattern;
    }

    @Override
    public List<String> execute() {
        return directory.search(pattern);
    }
}