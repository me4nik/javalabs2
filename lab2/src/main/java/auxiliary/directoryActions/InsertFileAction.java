package auxiliary.directoryActions;

import auxiliary.FSAction;
import filesystem.Directory;
import filesystem.File;

public class InsertFileAction implements FSAction<Boolean> {
    private final File file;
    private final Directory directory;

    public InsertFileAction(Directory directory, File file) {
        this.file = file;
        this.directory = directory;
    }


    @Override
    public Boolean execute() {
        try {
            directory.insertFile(file);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}