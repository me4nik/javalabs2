package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.Directory;
import filesystem.File;

public class MoveAction implements FSAction<Boolean> {
    private final Directory directory;
    private final File file;

    public MoveAction(File file, Directory directory) {
        this.directory = directory;
        this.file = file;
    }

    @Override
    public Boolean execute() {
        try {
            file.move(directory);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
