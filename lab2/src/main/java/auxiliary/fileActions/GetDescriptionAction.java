package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.File;

public class GetDescriptionAction implements FSAction<String> {
    private final File file;

    public GetDescriptionAction(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getDescription();
    }
}
