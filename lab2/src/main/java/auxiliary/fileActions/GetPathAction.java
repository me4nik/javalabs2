package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.File;

public class GetPathAction  implements FSAction<String> {
    private final File file;

    public GetPathAction(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getPath();
    }
}