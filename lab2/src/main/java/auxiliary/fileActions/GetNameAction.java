package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.File;

public class GetNameAction implements FSAction<String> {
    private final File file;

    public GetNameAction(File file) {
        this.file = file;
    }

    @Override
    public String execute() {
        return file.getName();
    }
}
