package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.File;

public class DeleteAction implements FSAction<Boolean> {
    private final File file;

    public DeleteAction(File file) {
        this.file = file;
    }

    @Override
    public Boolean execute() {
        try {
            file.delete();
            return true;
        }catch (Exception e){
            return false;
        }
    }
}