package auxiliary.fileActions;

import auxiliary.FSAction;
import filesystem.Directory;
import filesystem.File;

public class GetParentDirectoryAction implements FSAction<Directory> {
    private final File file;

    public GetParentDirectoryAction(File file) {
        this.file = file;
    }

    @Override
    public Directory execute() {
        return file.getParentDirectory();
    }
}
