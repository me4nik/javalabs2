package filesystem;

import com.fasterxml.jackson.databind.ObjectMapper;
import filesystem.methods.CountTask;
import filesystem.methods.SearchTask;
import filesystem.methods.SortTask;
import filesystem.methods.TreeTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class Directory extends File {
    private List<File> children;

    public Directory(String name) {
        super(name);
        children = new ArrayList<>();
    }

    public List<File> getContent() {
        try {
            mutex.lock();
            return children;
        } finally {
            mutex.unlock();
        }
    }

    public void insertFile(File child) throws Exception {
        try {
            mutex.lock();
            children.add(child);
            child.setParentDirectory(this);
        } finally {
            mutex.unlock();
        }
    }

    public void deleteFile(File child) {
        try {
            mutex.lock();
            children.remove(child);
        } finally {
            mutex.unlock();
        }
    }

    public List<String> search(String pattern) {
        return new ForkJoinPool().invoke(new SearchTask(this, pattern));
    }

    public Long count(boolean recursive){
        if (recursive) {
            return new ForkJoinPool().invoke(new CountTask(this));
        } else {
            return (long) getContent().size();
        }
    }

    public String tree() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(new ForkJoinPool().invoke(new TreeTask(this)));
    }

    public List<File> sort() {
        List<File> sorted = new ArrayList<>(this.children);
        new ForkJoinPool().invoke(new SortTask(sorted));
        return sorted;
    }
}