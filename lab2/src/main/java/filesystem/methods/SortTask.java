package filesystem.methods;
import filesystem.File;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class SortTask extends RecursiveAction {
    private List<File> list;
    private final int left;
    private final int right;

    public SortTask(List<File> list) {
        this(list, 0, list.size() - 1);
    }

    public SortTask(List<File> list, int left, int right) {
        this.list = list;
        this.left = left;
        this.right = right;
    }

    @Override
    protected void compute() {
        if (left < right) {
            int pivotIndex = partition(list, left, right);
            SortTask t1 = new SortTask(list, left, pivotIndex - 1);
            SortTask t2 = new SortTask(list, pivotIndex + 1, right);
            t1.fork();
            t2.compute();
            t1.join();
        }
    }

    int partition(List<File> list, int p, int r) {
        int i = p - 1;
        File x = list.get(r);
        for (int j = p; j < r; j++) {
            if (list.get(j).getName().compareTo(x.getName()) < 0) {
                i++;
                Collections.swap(list, i, j);
            }
        }
        i++;
        Collections.swap(list, i, r);
        return i;
    }
}
