package filesystem.methods;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import filesystem.Directory;
import filesystem.File;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class TreeTask extends RecursiveTask<JsonNode> {
    private final Directory node;
    ObjectMapper mapper = new ObjectMapper();

    public TreeTask(Directory node) {
        this.node = node;
    }

    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        List<TreeTask> subTasks = new LinkedList<>();

        for (File child : node.getContent()) {
            JsonNode jNode = ToJson(child);
            if (child instanceof Directory) {
                TreeTask task = new TreeTask((Directory) child);
                task.fork();
                subTasks.add(task);
            }else {
                root.add(jNode);
            }
        }

        for (TreeTask task : subTasks) {
            JsonNode jsonNode = task.join();
            root.add(jsonNode);
        }

        ObjectNode result = mapper.createObjectNode();
        result.put(node.getName(), root);

        return result;
    }

    private JsonNode ToJson(File node){
        ObjectNode tmp = mapper.createObjectNode();
        tmp.put(node.getName(), node.getDescription());
        return tmp;
    }
}