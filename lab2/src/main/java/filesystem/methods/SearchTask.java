package filesystem.methods;

import filesystem.Directory;
import filesystem.File;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class SearchTask extends RecursiveTask<List<String>> {
    private final Directory node;
    private final String pattern;

    public SearchTask(Directory node, String pattern) {
        this.node = node;
        this.pattern = pattern;
    }

    @Override
    protected List<String> compute() {
        List<String> path = new ArrayList<>();
        List<SearchTask> subTasks = new LinkedList<>();

        for (File child : node.getContent()) {
            if(child.getName().contains(pattern))
                path.add(child.getPath());
            if (child instanceof Directory) {
                SearchTask task = new SearchTask((Directory) child, pattern);
                task.fork();
                subTasks.add(task);
            }
        }

        for (SearchTask task : subTasks) {
            path.addAll(task.join());
        }

        return path;
    }
}
