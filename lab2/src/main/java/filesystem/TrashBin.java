package filesystem;

import java.util.ArrayList;
import java.util.List;

public class TrashBin extends File {
    private List<File> children;
    private static final int MAX_ELMS = 1000;

    public TrashBin(String name) {
        super(name);
        children = new ArrayList<>();
    }

    public void insertFile(File child) {
        try {
            mutex.lock();
            if (children.size() >= MAX_ELMS) {
                clean();
            }
            children.add(child);
            if (child.getParentDirectory() != null)
                child.getParentDirectory().deleteFile(child);
        } finally {
            mutex.unlock();
        }
    }


    public void restoreFile(File child) {
        try {
            mutex.lock();
            children.remove(child);
            child.getParentDirectory().insertFile(child);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mutex.unlock();
        }
    }

    public void clean() {
        try {
            mutex.lock();
            children.clear();
        } finally {
            mutex.unlock();
        }
    }

    public List<File> getContent() {
        try {
            mutex.lock();
            return children;
        } finally {
            mutex.unlock();
        }
    }
}
