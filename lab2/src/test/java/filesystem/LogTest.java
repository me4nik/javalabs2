package filesystem;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

class LogTest {

    @Test
    void writeAndReadTest() {
        Log file = new Log("data.log");
        assertEquals("", file.read());

        String[] lines = new String[] {"first", "second", "third"};
        for (String line: lines) {
            file.appendLine(line);
        }

        String expectedContent = String.join("\n", lines);
        assertEquals(expectedContent, file.read());
        assertEquals(expectedContent, file.read());
    }

    @Test
    void moveTest() throws Exception {
        Directory root = new Directory("root");
        Directory toMove = new Directory("dir");
        Log log = new Log("text.txt");
        root.insertFile(log);
        log.move(toMove);
        assertEquals(log.getParentDirectory(), toMove);
    }
}