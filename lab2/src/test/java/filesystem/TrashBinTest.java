package filesystem;

import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class TrashBinTest {

    @Test
    void getContentEmptyTest() {
        TrashBin root = new TrashBin("Bin");
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    void cleanTest() {
        TrashBin bin = new TrashBin("Bin");
        File binFile = new BinaryFile("text.txt");
        File logfile = new Log("log");
        bin.insertFile(binFile);
        bin.insertFile(logfile);
        bin.clean();
        assertEquals(Collections.emptyList(), bin.getContent());
    }

    @Test
    void insertAndGetContentTest() {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("text.txt");
        TrashBin bin = new TrashBin("Bin");

        try {
            root.insertFile(binFile);
            bin.insertFile(binFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(Collections.singletonList(binFile), bin.getContent());
        bin.restoreFile(binFile);
        assertEquals(Collections.singletonList(binFile), root.getContent());
    }

    @Test
    void insertAndDeleteTest() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        File bufFile  = new Buffer("buffer_file.bf", 10);
        File binFile = new BinaryFile("text.txt");

        try {
            root.insertFile(dataDir);
            dataDir.insertFile(bufFile);
            dataDir.insertFile(binFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        bufFile.delete();
        assertEquals(Collections.singletonList(binFile), dataDir.getContent());

        dataDir.delete();
        assertEquals(Collections.emptyList(), root.getContent());
    }
}