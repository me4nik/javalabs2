import auxiliary.Controller;
import auxiliary.directoryActions.*;
import auxiliary.fileActions.*;
import filesystem.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

class FunctionsTest {

    @Test
    void countTest() {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("text.txt");
        File bufFile = new Buffer("buffer_file.bf", 10);
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        File logFile = new Log("logs.log");

        try {
            root.insertFile(binFile);
            root.insertFile(bufFile);
            root.insertFile(dataDir);
            dataDir.insertFile(logsDir);
            logsDir.insertFile(logFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(root.count(true), 5);
        assertEquals(root.count(false), 3);
    }

    @Test
    void searchTest() {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("text.txt");
        File bufFile = new Buffer("buffer_file.bf", 10);
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        File logFile = new Log("logs.log");

        try {
            root.insertFile(binFile);
            root.insertFile(bufFile);
            root.insertFile(dataDir);
            dataDir.insertFile(logsDir);
            logsDir.insertFile(logFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(root.search("data"), Collections.singletonList(dataDir.getPath()));
        assertEquals(Arrays.asList(logsDir.getPath(), logFile.getPath()), root.search("logs"));
    }

    @Test
    void treeTest() throws IOException {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("text.txt");
        File bufFile = new Buffer("buffer_file.bf", 10);
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        File logFile = new Log("logs.log");

        try {
            root.insertFile(binFile);
            root.insertFile(bufFile);
            root.insertFile(dataDir);
            dataDir.insertFile(logsDir);
            logsDir.insertFile(logFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        root.tree();
    }

    @Test
    void sortTest() {
        Directory root = new Directory("root");
        File binFile = new BinaryFile("text.txt");
        File bufFile = new Buffer("buffer_file.bf", 10);
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        File logFile = new Log("logs.log");

        try {
            root.insertFile(binFile);
            root.insertFile(bufFile);
            root.insertFile(dataDir);
            root.insertFile(logsDir);
            root.insertFile(logFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(root.sort(), Arrays.asList(bufFile, dataDir, logsDir, logFile, binFile));
    }

    @Test
    void GeneratorTest() throws ExecutionException, InterruptedException {
        Directory root = new Directory("root");
        Controller controller = new Controller(10);
        Random random = new Random();

        for (int i = 0; i < 50; i++) {

            Directory dir = root;

            if (random.nextInt(50) % 2 == 0) {
                dir = new Directory("Dir" + i);
                controller.submitFSAction(new InsertFileAction(root, dir));
            }
            if (random.nextInt(50) % 3 == 0) {
                BinaryFile binaryFile = new BinaryFile("Binary" + i);
                controller.submitFSAction(new InsertFileAction(dir, binaryFile));
                Future<String> future = controller.submitFSAction(new GetNameAction(binaryFile));
                System.out.println("Name: " + future.get());
                future = controller.submitFSAction(new GetPathAction(binaryFile));
                System.out.println("Path: " + future.get());
                future = controller.submitFSAction(new GetDescriptionAction(binaryFile));
                System.out.println("Description: " + future.get());
            }
            if (random.nextInt(50) % 3 == 0) {
                Buffer buffer = new Buffer("Buffer" + i, i);
                controller.submitFSAction(new InsertFileAction(dir, buffer));
                if (i % 2 == 0) {
                    System.out.println("Delete " + controller.submitFSAction(new GetNameAction(buffer)).get());
                    controller.submitFSAction(new DeleteAction(buffer));
                }
            }
            if (random.nextInt(50) % 3 == 0) {
                Log log = new Log("Log" + i);
                controller.submitFSAction(new InsertFileAction(dir, log));
                if (random.nextInt(50) % 3 == 0) {
                    Directory subDir = new Directory("SubDir2_" + i);
                    controller.submitFSAction(new InsertFileAction(dir, subDir));
                    controller.submitFSAction(new MoveAction(log, subDir));
                }
            }
        }

        String dirName = "Dir1";
        Future<List<String>> searchFuture = controller.submitFSAction(new SearchAction(root, dirName));
        for (String search : searchFuture.get()) {
            System.out.println(search);
        }

        Future<Long> countFuture = controller.submitFSAction(new CountAction(root, true));
        countFuture.get();

        Future<String> treeFuture = controller.submitFSAction(new TreeAction(root));
        treeFuture.get();

        Future<List<File>> sortFuture = controller.submitFSAction(new SortAction(root));
        for (File f : sortFuture.get()) {
            f.getName();
        }

        controller.stop();
    }
}